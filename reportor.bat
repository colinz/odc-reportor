
@echo off

echo SOAPUI TESTING...
echo ----------------------------------------
call testrunner -a -f"%1\REPORTS" -I "%2" 

echo GENERATE REPORT...
echo ----------------------------------------
java -cp reportor-*.jar colinz.reportor.Bootstrap "%1\REPORTS" "%1"

echo DONE!!
