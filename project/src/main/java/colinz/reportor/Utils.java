package colinz.reportor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

	public static void filter(File path, String regex, Handler hand) {
		if (path.isDirectory()) {
			File[] list = path.listFiles();
			for (File file : list) {
				filter(file, regex, hand);
			}
		} else {
			Pattern pattern = Pattern.compile(regex);
			Matcher mather = pattern.matcher(path.getName());
			if (mather.matches()) {
				hand.hand(path);
			}
		}
	}

	public static void mkdir(String path) {
		File f = new File(path);
		if (!f.exists()) {
			f.getParentFile().mkdirs();
		}
	}

	public static String slurp(File file) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), "UTF-8"));
		StringBuilder builder = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null) {
			builder.append(line + "\n");
		}
		reader.close();
		return builder.toString();
	}

	public static void write(File file, String txt) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(file), "UTF-8"));
		writer.write(txt);
		writer.flush();
		writer.close();
	}

}