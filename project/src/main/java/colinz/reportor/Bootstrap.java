package colinz.reportor;

import java.io.File;

public class Bootstrap {

	public static void main(String[] args) {

		String reportsDir = args[0];
		String outputDir = args[1];

		if (args.length != 2) {
			new RuntimeException("Incorrect parameter.\n"
					+ "Usage: java -cp reporter-*.jar "
					+ "colinz.report.Bootstrap " + "REPORTS_DIR OUTPUT_DIR");
		}

		File path = new File(reportsDir);

		if (!path.exists()) {
			new RuntimeException("Incorrect reports location: " + args[0]);
		}

		String pattern = ".*\\.txt";
		Handler handler = new ParserHandler(outputDir);

		Utils.filter(path, pattern, handler);

	}

}
