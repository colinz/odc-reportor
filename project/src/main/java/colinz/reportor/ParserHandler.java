package colinz.reportor;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParserHandler implements Handler {

	private String output;

	public ParserHandler(String output) {
		this.output = output;
	}

	public void hand(File file) {
		try {
			String txt = Utils.slurp(file);
			Thread thread = new Thread(new ParseFile(file, txt));
			thread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	class ParseFile implements Runnable {

		private File file;
		private String txt;

		public ParseFile(File file, String txt) {
			this.file = file;
			this.txt = txt;
		}

		public void run() {

			String status = this.getStatus();

			if (!Constant.STATUS_OK.equals(status)) {
				// request failed
				System.out.println(file.getName());
				return;
			}

			String response = this.getResponse();
			if (response == null) {
				// FIXME no response or timeout??
				return;
			}

			String name = file.getName();
			Matcher mather = REGEX_FILENAME.matcher(name);
			if (mather.matches()) {

				String testSuite = mather.group(1);
				String javaFile = mather.group(2);
				String testCase = mather.group(3);
				String order = mather.group(4);

				// GI702099 --> G_I702099.java
				javaFile = javaFile.substring(0, 1) + "_"
						+ javaFile.substring(1);

				// status${order}
				int statusOrder = 0;
				if (order != null && !order.trim().equals("")) {
					statusOrder = Integer.parseInt(order) - 1;
				}

				String path = output + "/" + testSuite + "/" + javaFile + "/"
						+ testCase;

				String statusN = path + "/status" + statusOrder + ".txt";

				Utils.mkdir(statusN);

				try {
					// save result
					Utils.write(new File(statusN), response);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		private final Pattern REGEX_FILENAME = Pattern.compile(
				"(.*)-(.*)-(\\d{3})(\\d?)-(\\d)-(\\w*).txt", Pattern.DOTALL);

		private final Pattern REGEX_STATUS = Pattern.compile(".*-(\\w*).txt",
				Pattern.DOTALL);

		private final Pattern REGEX_RESPONSE = Pattern
				.compile(
						".*(<soapenv:Envelope "
								+ "xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
								+ "[\\s\\S]*?" + "</soapenv:Envelope>).*",
						Pattern.DOTALL);

		private String getStatus() {
			String status = null;
			Matcher mather = REGEX_STATUS.matcher(file.getName());
			if (mather.matches()) {
				status = mather.group(1);
			}
			return status;
		}

		private String getResponse() {
			String response = null;
			Matcher mather = REGEX_RESPONSE.matcher(txt);
			if (mather.matches()) {
				response = mather.group(1);
			}
			return response;
		}
	}
}