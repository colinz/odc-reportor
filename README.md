# Reportor

Reportor用来自动运行SoapUI的测试用例，并将SoapUI成测试结果进行解析，生成测试报告。它的主要目的是将测试过程自动化，自动生成报告，提高工作效率。特别是当代码频繁变动的时候，它能快速完成回归测试。

## Features

+ 自动运行SoapUI测试
+ 生成测试报告
+ 回归测试

## Usage

1. 下载代码
2. 将SOAPUI_HOME\bin添加到PATH路径
2. 修改示例文件fa.bat中的变量

    > set report_dir=D:\Work\Output\FA  
    > set project_file=D:\Work\SOAPUI\FA--soapui-project.xml

3. 运行fa.bat，或者运行all.bat (fa.bat已被加入到all.bat中)
4. 编写更多模块的bat文件，如fd.bat。每个模块可单独运行也可加入到all.bat中运行。
5. HAVE FUN :)

输出报告目录结构示例：  

![输出报告目录结构示例][sample]

## Contact

+ 张露兵 (Colin Zhang)
+ Email: zhanglubing927[at]163.com
+ Blog: [http://zhanglubing.github.com][blog]

[blog]: http://zhanglubing.github.com
[sample]: https://bitbucket.org/colinz/odc-reportor/downloads/output.png  "输出报告目录结构示例"